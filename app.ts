import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import dotenv from "dotenv";
import cors from "cors";

import indexRouter from "./routes";

dotenv.config();
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
if (process.env.NODE_ENV === "development") app.use(cors());

app.use("/api", indexRouter);
if (process.env.NODE_ENV === "development") {
  app.get("/", (req, res) => res.redirect("http://localhost:3001/"));
} else {
  app.use(express.static(path.join(__dirname, "react")));
  app.get("/*", function (req, res) {
    res.sendFile(path.join(__dirname, "react/index.html"), function (err) {
      if (err) {
        res.status(500).send(err);
      }
    });
  });
}

/* final catch-all route to index.html defined last */
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "react", "index.html"));
});

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
