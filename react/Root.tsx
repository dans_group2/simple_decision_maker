import React from "react";
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import Home from "./pages/Home";
import MainLayout from "./layouts/MainLayout";
import Result from "./pages/Result";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <MainLayout>
        <Home />
      </MainLayout>
    ),
  },
  {
    path: "result",
    element: (
      <MainLayout>
        <Result />
      </MainLayout>
    ),
  },
]);

const Root = ({}) => {
  return (
    <>
      <RouterProvider router={router} />
      <Outlet />
    </>
  );
};

export default Root;
