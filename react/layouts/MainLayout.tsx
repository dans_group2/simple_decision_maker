import React from "react";
import HeaderBar from "../components/HeaderBar/HeaderBar";
import Box from "@mui/material/Box";

type MainLayoutProps = {
  children: React.ReactElement;
};

export default function (props: MainLayoutProps) {
  return (
    <Box sx={{ padding: 0, margin: 0 }}>
      <HeaderBar />
      {props.children}
    </Box>
  );
}
