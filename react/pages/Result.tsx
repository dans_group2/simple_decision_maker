import React, { useEffect, useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import Button from "@mui/material/Button";
import { SxProps } from "@mui/system";
import { Theme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { TextField } from "@mui/material";

export type Submission = { question: string; options: string[] };

export default function Result() {
  let [searchParams] = useSearchParams();

  const submission: Submission = JSON.parse(
    searchParams.get("submissionJSON") || "{}"
  );
  const { question } = submission;
  const options = parseOptions(submission);
  function submitHandler(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    getNewResult();
  }

  const [result, setResult] = useState<number>(-1);

  function getNewResult() {
    setResult(Math.floor(Math.random() * options.length));
  }

  function applyResultStyling(index: number): SxProps<Theme> {
    if (index === result) {
      return {
        border: "5px solid red",
        borderRadius: 10,
        padding: 2,
        fontColor: "red",
      };
    } else {
      return {};
    }
  }

  function parseOptions(submission: Submission): string[] {
    const filteredOptions = submission.options.filter(
      (option) => option.length > 0
    );
    if (filteredOptions.length === 0) {
      return ["yes", "no"];
    } else {
      return filteredOptions;
    }
  }

  useEffect(getNewResult, []);

  return (
    <Box marginTop={4}>
      <form onSubmit={submitHandler} style={{ height: "100%" }}>
        <Box
          height={"100%"}
          display={"flex"}
          flexDirection="column"
          justifyContent={"flex-start"}
        >
          <Typography
            variant="h1"
            align="center"
            sx={{ fontSize: "3rem", marginBottom: 3 }}
          >
            {question || "Just pick:"}
          </Typography>

          {options.map((option, index) => (
            <Box
              width="80%"
              margin={"0 auto"}
              sx={{ ...applyResultStyling(index) }}
            >
              <TextField
                label={`Option: ${index + 1}`}
                key={index}
                value={options[index]}
                variant={"outlined"}
                size={"small"}
                sx={{ marginY: 2, width: "100%" }}
                disabled={true}
              />
            </Box>
          ))}
          <Button
            type="submit"
            variant={"contained"}
            sx={{ height: 50, marginTop: 5 }}
          >
            Ask again
          </Button>
        </Box>
      </form>
      <Link to="/">
        <Button fullWidth sx={{ marginTop: 2 }} variant={"outlined"}>
          Ask a different question
        </Button>
      </Link>
    </Box>
  );
}
