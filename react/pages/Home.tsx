import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { TextField } from "@mui/material";
import Button from "@mui/material/Button";

export default function Home() {
  const navigate = useNavigate();

  function submitHandler(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const submissionJSON = { question, options };
    navigate(`/result?submissionJSON=${JSON.stringify(submissionJSON)}`);
  }

  const [question, setQuestion] = useState<string>("");

  const [options, setOptions] = useState<string[]>(["", "", ""]);

  function setOption(
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    index: number
  ) {
    const value = event.target.value;
    const newOptions = [...options];
    newOptions[index] = value;

    setOptions(newOptions);
  }

  function renderProperAmountOfOptions() {
    let optionsFilled = 0;

    options.forEach((option) => {
      if (option.length > 0) {
        optionsFilled += 1;
      }
    });

    if (optionsFilled >= 3 && optionsFilled === options.length) {
      setOptions((prevState) => [...prevState, ""]);
      return;
    }

    const emptyOptions = options.length - optionsFilled;

    if (emptyOptions > 1 && options.length != 3) {
      const newArray: string[] = [...options.filter((e) => e.length > 0)];
      for (let i = 0; i < 3; i++) {
        if (!newArray[i]) newArray.push("");
      }
      setOptions(newArray);
    }
  }

  useEffect(() => {
    renderProperAmountOfOptions();
  }, [options]);

  return (
    <Box marginTop={4}>
      <form onSubmit={submitHandler} style={{ height: "100%" }}>
        <Box
          height={"100%"}
          display={"flex"}
          flexDirection="column"
          justifyContent={"flex-start"}
        >
          <>
            {" "}
            <Typography variant="h1" align="left" sx={{ fontSize: "3rem" }}>
              What is on your mind?
            </Typography>
            <TextField
              sx={{ margin: 3 }}
              value={question}
              onChange={(event) => setQuestion(event.target.value)}
              label="Enter your query"
              variant="filled"
            />
          </>

          <Typography variant="h2" align="left" sx={{ fontSize: 30 }}>
            Options
          </Typography>
          {options.map((option, index) => (
            <Box width="80%" margin={"0 auto"}>
              <TextField
                label={`Option: ${index + 1}`}
                onChange={(args) => setOption(args, index)}
                key={index}
                value={options[index]}
                variant={"outlined"}
                size={"small"}
                sx={{ marginY: 2, width: "100%" }}
              />
            </Box>
          ))}
          <Button
            type="submit"
            variant={"contained"}
            sx={{ height: 50, marginTop: 5 }}
          >
            Get Answer
          </Button>
        </Box>
      </form>
    </Box>
  );
}
