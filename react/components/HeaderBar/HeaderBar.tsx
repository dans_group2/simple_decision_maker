import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import MediumAndHigherToolBar from "../MediumAndHigherToolBar/MediumAndHigherToolBox";
import MediumAndLowerToolBar from "../MediumAndLowerToolBar/MediumAndLowerToolBar";

function HeaderBar() {
  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Box sx={{ display: { md: "flex" }, width: "100%" }}>
          <MediumAndHigherToolBar />
        </Box>

        <Box sx={{ display: { xs: "flex", md: "none" }, width: "100%" }}>
          <MediumAndLowerToolBar />
        </Box>
      </Container>
    </AppBar>
  );
}
export default HeaderBar;
